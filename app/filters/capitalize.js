/**
 * Created by vlad on 14/02/16.
 */
(function () {
    'use strict';
    angular.module('searchUserApp')
        .filter('capitalize', function () {
            return function (input) {
                return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
            }
        })
})();