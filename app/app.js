(function() {
    'use strict';
    angular.module('searchUserApp', ['ngRoute'])
        .config(function($locationProvider, $routeProvider){

            $routeProvider
                .when('/',{
                    templateUrl: 'app/views/search.html',
                    controller: 'MainCtrl'
                })
                .when('/details', {
                    templateUrl: 'app/views/details.html',
                    controller: 'DetailsCtrl'
                })
                .otherwise({redirectTp: '/'});
        });


})();   
