/**
 * Created by vlad on 14/02/16.
 */
(function(){
    'use strict';
    angular.module('searchUserApp')
        .controller('DetailsCtrl', ['$scope', 'SaveService', function($scope, SaveService){
            console.log("DetailsCtrl");
            $scope.details = SaveService.get();
        }]);
})();