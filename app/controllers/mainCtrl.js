(function () {
    'use strict';
    angular.module('searchUserApp')
        .controller('MainCtrl', ['$scope', '$location', 'SearchService', 'DataService', 'SaveService',
            function ($scope, $location, SearchService, DataService, SaveService) {
                console.log("MainCtrl");
                $scope.findUserByName = function () {
                    var user = SearchService.findUserByName($scope.search, DataService.getUsers());
                    if (!user) {
                        $scope.result = 'No user found';
                    } else {
                        $scope.user = user;
                        var userPosts = SearchService.findPost(user.id, DataService.getPosts());
                        if (!userPosts) {
                            $scope.result = 'User has not had any posts';
                        } else {
                            $scope.result = userPosts;
                        }
                    }
                };

                $scope.getDetails = function (post) {
                    SaveService.set({post: post, user: $scope.user});
                    $location.url('/details');
                }
            }]);
})();