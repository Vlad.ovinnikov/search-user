/**
 * Created by vlad on 14/02/16.
 */
(function () {
    'use strict';
    angular.module('searchUserApp')
        .service('SearchService', function () {
            return {
                findUserByName: function (input, users) {
                    var i = 0,
                        len = users.length;
                    for (i; i < len; i++) {
                        if (input === users[i].name)
                            return users[i];
                    }
                    return null;
                },
                findPost: function (userId, posts) {
                    var i = 0,
                        res = [],
                        len = posts.length;
                    for (i; i < len; i++) {
                        if(userId === posts[i].userId)
                        res.push(posts[i]);
                    }
                    return res;
                }
            }
        });
})();