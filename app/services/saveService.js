/**
 * Created by vlad on 14/02/16.
 */
(function () {
    'use strict';
    angular.module('searchUserApp')
        .service('SaveService', function () {
            var savedData = {};
            return {
                set: function (data) {
                    savedData = data;
                },
                get: function () {
                    return savedData
                }
            }
        });
})();